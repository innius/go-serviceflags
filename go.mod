module bitbucket.org/innius/go-serviceflags

go 1.12

require (
	github.com/aws/aws-sdk-go v1.20.2
	github.com/jpillora/backoff v0.0.0-20180909062703-3050d21c67d7
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980 // indirect
)
