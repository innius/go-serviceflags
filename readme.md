#go-service-flags

Simple golang package to read service flags. 

A service flag can be an environment variable of an AWS SSM Parameter. 

Usage: 
```go

flags := serviceflags.New(
	providers.NewSSMParameterStoreProvider(ssmClient, providers.SSMParameterStoreProviderConfig{Interval: 10 * time.Second}),
	&providers.EnvironmentVariableProvider{})


var featureEnabled = flags.Bool("/foo/bar/baz") // returns a pointer which underlying value might change

if featureEnabled {
	...
} else {
	...
}

```


 