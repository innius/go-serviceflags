package providers

import (
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	. "github.com/smartystreets/goconvey/convey"
)

type ssmMock struct {
	ssmiface.SSMAPI
	lastValue string
}

func (m *ssmMock) GetParameter(*ssm.GetParameterInput) (*ssm.GetParameterOutput, error) {
	return &ssm.GetParameterOutput{
		Parameter: &ssm.Parameter{
			Value: aws.String(m.lastValue),
		},
	}, nil
}

func TestSSMProvider(t *testing.T) {
	Convey("SSM Parameter Store Provider", t, func() {
		mock := &ssmMock{
			lastValue: "initial",
		}
		p := NewSSMParameterStoreProvider(mock, SSMParameterStoreProviderConfig{Interval: 5 * time.Millisecond})

		callback := false
		Convey(" update the store", func(c C) {
			v := p.Get("/foo", func(value Value) {
				c.So(value.String(), ShouldEqual, "updated")
				callback = true
			})
			c.So(v, ShouldEqual, "initial")
		})

		mock.lastValue = "updated"
		time.Sleep(10 * time.Millisecond)
		So(callback, ShouldBeTrue)
	})

}
