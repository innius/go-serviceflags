package providers

import (
	"os"
	"strings"
)

type EnvironmentVariableProvider struct{}

func (vs *EnvironmentVariableProvider) Get(key string, watch ...func(Value)) Value {
	// "/chair/invoice-service/foobar"
	stripped := key
	s := strings.Split(strings.ToUpper(stripped), "/")
	l := len(s)
	if l > 0 {
		stripped = s[l-1]
	}
	v := Value(os.Getenv(stripped))
	if v.String() != "" {
		return v
	}
	return Value(os.Getenv(key))
}
