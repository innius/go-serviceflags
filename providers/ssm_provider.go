package providers

import (
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"github.com/jpillora/backoff"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type SSMParameterStoreProviderConfig struct {
	Interval time.Duration
}

// observe the specified keys and invoke callback if value changes
func (s *sspParameterStoreProvider) observe(w *watch) {
	v, err := s.get(w.key)
	if err != nil {
		logrus.Errorf("could not get value from ssm; %+v", err)
		time.AfterFunc(w.Duration(), func() {
			s.observe(w)
		})
		return
	}

	if v != w.lastValue {
		logrus.Infof("value change for %s: %s -> %s", w.key, w.lastValue, v)
		w.lastValue = v
		w.callback(v)
		w.Reset()
	}
	time.AfterFunc(s.Interval, func() {
		s.observe(w)
	})
}

func NewSSMParameterStoreProvider(ssmapi ssmiface.SSMAPI, config SSMParameterStoreProviderConfig) ValueStore {
	store := &sspParameterStoreProvider{
		ssmapi: ssmapi,

		Interval: config.Interval,
	}

	return store
}

type watch struct {
	key string
	*backoff.Backoff
	callback  func(Value)
	lastValue Value
}

type sspParameterStoreProvider struct {
	ssmapi   ssmiface.SSMAPI
	lock     sync.Mutex
	watches  map[string][]*watch
	Interval time.Duration
}

func (s *sspParameterStoreProvider) get(key string) (Value, error) {
	resp, err := s.ssmapi.GetParameter(&ssm.GetParameterInput{Name: aws.String(key)})
	if err != nil {
		return "", errors.Wrap(err, "get ssm parameter value")
	}
	return Value(aws.StringValue(resp.Parameter.Value)), nil
}

func (s *sspParameterStoreProvider) Get(key string, watches ...func(Value)) Value {
	currentValue, err := s.get(key)
	if err != nil {
		logrus.Errorf("could not get ssm parameter; %+v", err)
	}

	if len(watches) > 0 {
		for _, w := range watches {
			watch := &watch{
				key:       key,
				callback:  w,
				lastValue: currentValue,
				Backoff:   &backoff.Backoff{Min: s.Interval, Max: 10 * time.Minute, Jitter: true},
			}

			time.AfterFunc(s.Interval, func() {
				s.observe(watch)
			})
		}
	}
	return currentValue
}
