package providers

type ValueStore interface {
	Get(key string, watch ...func(Value)) Value
}

type Value string

func (v Value) String() string {
	return string(v)
}

