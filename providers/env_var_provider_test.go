package providers

import (
	"os"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestEnvironmentVariableStore(t *testing.T) {
	Convey("EnvironmentVariableProvider", t, func() {
		p := &EnvironmentVariableProvider{}
		v := Value("yo")

		Convey("Simple environment var", func() {
			os.Setenv("baz", v.String())
			So(p.Get("baz"), ShouldEqual, v)
		})
		Convey("an ssm parameter path", func() {
			os.Setenv("BAZ", v.String())
			So(p.Get("/observe/bar/baz"), ShouldEqual, v)
		})
	})
}
