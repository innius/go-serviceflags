package serviceflags

import (
	"testing"

	"bitbucket.org/innius/go-serviceflags/providers"

	. "github.com/smartystreets/goconvey/convey"
)

type testStore struct {
	initial string
	watches []func(value providers.Value)
}

func (vs *testStore) Get(key string, watches ...func(providers.Value)) providers.Value {
	vs.watches = watches
	return providers.Value(vs.initial)
}

func (vs *testStore) set(v string) {
	for _, watch := range vs.watches {
		watch(providers.Value(v))
	}
}

func TestFlags(t *testing.T) {
	Convey("Test the serviceflags", t, func() {
		Convey("string flag", func() {
			store := &testStore{initial: "initial"}
			factory := New(store)

			f := factory.String("/foo/bar/baz")

			So(f.Value(), ShouldEqual, "initial")

			store.set("updated")

			So(f.Value(), ShouldEqual, "updated")
		})
		Convey("bool flag", func() {
			store := &testStore{initial: ""}
			factory := New(store)

			f := factory.Bool("/foo/bar/baz/boolean")

			So(f.Value(), ShouldEqual, false)

			store.set("true")

			So(f.Value(), ShouldEqual, true)
		})
	})
}
