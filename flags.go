package serviceflags

import (
	"strconv"

	"bitbucket.org/innius/go-serviceflags/providers"

	"github.com/aws/aws-sdk-go/aws"
)

func New(stores ...providers.ValueStore) *Factory {
	if len(stores) == 0 {
		stores = []providers.ValueStore{&providers.EnvironmentVariableProvider{}}
	}
	return &Factory{
		stores: stores,
	}
}

type Factory struct {
	stores []providers.ValueStore
}

func (ff *Factory) String(key string) StringValue {
	v := stringValue{value: aws.String("")}
	newStringValue(key, ff.stores, &v)
	return &v
}

func (ff *Factory) Bool(key string) BoolValue {
	v := boolValue{value: aws.Bool(false)}
	newBoolValue(key, ff.stores, &v)
	return &v
}

type StringValue interface {
	Value() string
}

type stringValue struct {
	value *string
}

func (s *stringValue) Value() string {
	if s == nil {
		return ""
	}
	return aws.StringValue(s.value)
}

func newStringValue(key string, sts []providers.ValueStore, s *stringValue) {
	for _, store := range sts {
		initial := store.Get(key, func(value providers.Value) {
			*s.value = value.String()
		})
		*s.value = initial.String()
	}
}

type BoolValue interface {
	Value() bool
}

type boolValue struct {
	value *bool
}

func (s *boolValue) Value() bool {
	if s == nil {
		return false
	}
	return aws.BoolValue(s.value)
}

func newBoolValue(key string, sts []providers.ValueStore, s *boolValue) {
	for _, store := range sts {
		initial := store.Get(key, func(value providers.Value) {
			b, _ := strconv.ParseBool(value.String())
			*s.value = b
		})
		b, _ := strconv.ParseBool(initial.String())
		*s.value = b
	}
}
